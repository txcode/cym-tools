<?php


namespace CymTools\Sms;


use CymTools\Kernel\ServiceContainer;

/**
 * Class Application
 *
 * @property AliSms\Client $ali
 */
class Application extends ServiceContainer
{
    protected $providers = [
        AliSms\ServiceProvider::class
    ];

}