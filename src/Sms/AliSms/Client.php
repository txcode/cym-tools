<?php


namespace CymTools\Sms\AliSms;


use CymTools\Kernel\ServiceClient;
use CymTools\Sms\Application;

class Client extends ServiceClient
{

    // 模板CODE
    private $templateCode;

    // 默认请求网关
    private $gateWay = "http://dysmsapi.aliyuncs.com/?";

    // 错误信息
    private $errorMessage;

    public function __construct($app)
    {
        parent::__construct($app);
    }

    /**
     * 发送短信
     * @param $mobile
     * @param $templateData
     * @return bool
     */
    public function sendSms($mobile, $templateData)
    {
        $params = [
            'SignName' => $this->config->signName,
            'Format' => 'JSON',
            'Version' => '2017-05-25',
            'AccessKeyId' => $this->config->accessKeyId,
            'SignatureVersion' => '1.0',
            'SignatureMethod' => 'HMAC-SHA1',
            'SignatureNonce' => uniqid (),
            'Timestamp' => gmdate ( 'Y-m-d\TH:i:s\Z' ),
            'Action' => 'SendSms',
            'TemplateCode' => $this->config->templateCode,
            'PhoneNumbers' => $mobile,
            'TemplateParam' => json_encode($templateData)
        ];

        $params ['Signature'] = $this->computeSignature($params, $this->config->accessKeySecret);
        $res = $this->curlData($this->gateWay.http_build_query($params) );
        if ($res['Code'] != 'OK'){
            $this->errorMessage = $this->errorInfo($res['Code']);
            return false;
        }
        return true;
    }

    public function curlData($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }

    /**
     * 签名
     *
     * @param array $parameters
     * @param string $accessKeySecret
     * @return string
     */
    private function computeSignature($parameters, $accessKeySecret)
    {
        ksort($parameters);
        $canonicalizedQueryString = '';
        foreach ( $parameters as $key => $value ) {
            $canonicalizedQueryString .= '&' . $this->percentEncode ( $key ) . '=' . $this->percentEncode ( $value );
        }
        $stringToSign = 'GET&%2F&' . $this->percentencode ( substr ( $canonicalizedQueryString, 1 ) );
        $signature = base64_encode ( hash_hmac ( 'sha1', $stringToSign, $accessKeySecret . '&', true ) );
        return $signature;
    }

    private function percentEncode($string) {
        $string = urlencode ( $string );
        $string = preg_replace ( '/\+/', '%20', $string );
        $string = preg_replace ( '/\*/', '%2A', $string );
        $string = preg_replace ( '/%7E/', '~', $string );
        return $string;
    }

    /**
     * @param string $status
     * @return string
     */
    private function errorInfo(string $status): string
    {
        $message = array (
            'isv.BUSINESS_LIMIT_CONTROL' => '短信发送频率超限',
            'isv.DAY_LIMIT_CONTROL' => '已经达到您在控制台设置的短信日发送量限额值',
            'isv.SMS_CONTENT_ILLEGAL' => '短信内容包含禁止发送内容,请修改短信文案',
            'isv.SMS_SIGN_ILLEGAL' => '签名禁止使用,请在短信服务控制台中申请符合规定的签名',
            'isp.RAM_PERMISSION_DENY' => 'RAM权限不足。,请为当前使用的AK对应子账号进行授权：AliyunDysmsFullAccess（管理权限）',
            'isv.OUT_OF_SERVICE' => '余额不足。余额不足时，套餐包中即使有短信额度也无法发送短信',
            'isv.PRODUCT_UN_SUBSCRIPT' => '未开通云通信产品的阿里云客户',
            'isv.PRODUCT_UNSUBSCRIBE' => '产品未开通',
            'isv.ACCOUNT_NOT_EXISTS' => '账户不存在，使用了错误的账户名称或AK',
            'isv.ACCOUNT_ABNORMAL' => '账户异常',
            'isv.SMS_TEMPLATE_ILLEGAL' => '短信模板不存在，或未经审核通过',
            'isv.SMS_SIGNATURE_ILLEGAL' => '签名不存在，或未经审核通过',
            'isv.INVALID_PARAMETERS' => '参数格式不正确',
            'isv.MOBILE_NUMBER_ILLEGAL' => '手机号码格式错误',
            'isv.MOBILE_COUNT_OVER_LIMIT' => '参数PhoneNumbers中指定的手机号码数量超出限制',
            'isv.TEMPLATE_MISSING_PARAMETERS' => '模版缺少变量',
            'isv.BLACK_KEY_CONTROL_LIMIT' => '黑名单管控',
            'isv.PARAM_LENGTH_LIMIT' => '参数超出长度限制',
            'isv.PARAM_NOT_SUPPORT_URL' => '不支持URL',
            'isv.AMOUNT_NOT_ENOUGH' => '账户余额不足',
            'SignatureDoesNotMatch' => '签名（Signature）加密错误',
        );
        if (isset ($message[$status])) {
            return $message[$status];
        }
        return $status;
    }

    /**
     * @return mixed
     */
    public function getTemplateCode()
    {
        return $this->templateCode;
    }

    /**
     * @param mixed $templateCode
     */
    public function setTemplateCode($templateCode): void
    {
        $this->templateCode = $templateCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }


    /**
     * @param mixed $errorMessage
     */
    public function setErrorMessage($errorMessage): void
    {
        $this->errorMessage = $errorMessage;
    }





}