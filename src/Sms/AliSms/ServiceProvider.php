<?php


namespace CymTools\Sms\AliSms;


use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $app)
    {

        $app['ali'] = function ($app) {
            return new Client($app);
        };
    }
}