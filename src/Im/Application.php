<?php


namespace CymTools\Im;


use CymTools\Kernel\ServiceContainer;

/**
 * Class Application
 * @property Account\Client $account
 * @property Relation\Client $relation
 * @property Group\Client $group
 */
class Application extends ServiceContainer
{
    protected $providers = [
        Account\ServiceProvider::class,
        Relation\ServiceProvider::class,
        Group\ServiceProvider::class
    ];
}