<?php

namespace CymTools\Im\Relation;
use CymTools\Im\Support\Host;
use CymTools\Im\Support\TLSSigAPIv2;
use CymTools\Kernel\ServiceClient;
use CymTools\Kernel\Support\Str;
use GuzzleHttp\Client as Http;
use Psr\Http\Message\ResponseInterface;

class Client extends ServiceClient
{
    // 默认管理员账号ID
    private $identifier = 'administrator';

    // 腾讯IM分配的SDK APPID
    private $appid;

    //腾讯IM分配的SDK SECRET
    private $secret;

    // HTTP请求
    private $http;

    /**
     * @var TLSSigAPIv2
     */
    private $TLSSignAPI;

    public function __construct($app)
    {
        parent::__construct($app);
        $this->http = new Http(['verify'=>false]);
        $this->TLSSignAPI = new TLSSigAPIv2($this->config->accessKeyId, $this->config->accessKeySecret);
    }

    /**
     * 添加好友
     * @param string $fromAccount  需要为该UserID添加好友
     * @param array $addFriendItem 好友结构体对象
     * @param string $addType 加好友方式 Add_Type_Single 表示单向加好友,Add_Type_Both 表示双向加好友 默认为双向加好友
     * @param int $forceAddFlags 管理员强制加好友标记：1表示强制加好友，0表示常规加好友方式
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function friendAdd(string $fromAccount, array $addFriendItem, string $addType = 'Add_Type_Both', int $forceAddFlags = 0): ResponseInterface
    {
        $url = $this->getUrlParameter(Host::FRIEND_ADD_URL);
        return $this->http->post($url, ['json'=>[
            'From_Account'=>$fromAccount,
            'AddFriendItem'=>$addFriendItem,
            'AddType'=>$addType,
            'ForceAddFlags'=>$forceAddFlags
        ]]);
    }

    /**
     * 删除好友
     * @param string $fromAccount 需要删除该 UserID 的好友
     * @param array $toAccount 待删除的好友的 UserID 列表，单次请求的 To_Account 数不得超过1000
     * @param string $deleteType 删除模式
     * @return ResponseInterface
     * @throws \Exception
     */
    public function friendDelete(string $fromAccount, array $toAccount, string $deleteType): ResponseInterface
    {
        $url = $this->getUrlParameter(Host::FRIEND_DELETE_URL);
        return $this->http->post($url, ['json'=>[
            'From_Account' => $fromAccount,
            'To_Account' => $toAccount,
            'DeleteType' => $deleteType
        ]]);
    }

    /**
     * 生成URL链接
     * @param $url string 请求链接
     * @param $user_id mixed|string 用户ID
     * @return string
     * @throws \Exception
     */
    private function getUrlParameter(string $url, string $user_id = null): string
    {
        $identifier = empty($user_id) ? $this->identifier: $user_id;
        $param = http_build_query([
            'sdkappid' => $this->config->accessKeyId,
            'identifier' => $identifier,
            'usersig' => $this->genUserSign($identifier),
            'random' => Str::randomInt(32),
            'contenttype' => 'json'
        ]);
        return $url.$param;
    }

    /**
     * 生成用户签名
     * @param $user_id string 用户ID
     * @return string
     * @throws \Exception
     */
    public function genUserSign(string $user_id)
    {
        return $this->TLSSignAPI->genUserSig($user_id);
    }
}