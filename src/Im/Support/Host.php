<?php

namespace CymTools\Im\Support;

class Host
{
    // 导入账号URL
    const ACCOUNT_IMPORT_URL = "https://console.tim.qq.com/v4/im_open_login_svc/account_import?";

    // 检测账号是否存在URL
    const ACCOUNT_CHECK_URL = "https://console.tim.qq.com/v4/im_open_login_svc/account_check?";

    // 设置资料URL
    const PORTRAIT_SET_URL = "https://console.tim.qq.com/v4/profile/portrait_set?";

    // 拉入黑名单URL
    const BLACK_LIST_ADD_URL = "https://console.tim.qq.com/v4/sns/black_list_add?";

    // 删除黑名单URL
    const BLACK_LIST_DELETE_URL = "https://console.tim.qq.com/v4/sns/black_list_delete?";

    // 拉取黑名单列表URL
    const GET_BLACK_LIST_URL = "https://console.tim.qq.com/v4/sns/black_list_get?";

    // 校验黑名单URL
    const BLACK_LIST_CHECK_URL = "https://console.tim.qq.com/v4/sns/black_list_check?";

    // 查询登陆状态URL
    const QUERY_STATUS_URL = "https://console.tim.qq.com/v4/openim/querystate?";

    // 添加好友URL
    const FRIEND_ADD_URL = "https://console.tim.qq.com/v4/sns/friend_add?";

    // 删除好友URL
    const FRIEND_DELETE_URL = "https://console.tim.qq.com/v4/sns/friend_delete?";

    // 创建群组URL
    const CREATE_GROUP_URL = "https://console.tim.qq.com/v4/group_open_http_svc/create_group?";

    // 修改群基础资料URL
    const MODIFY_GROUP_BASE_INFO_URL = "https://console.tim.qq.com/v4/group_open_http_svc/modify_group_base_info?";

    // 添加群成员URL
    const ADD_GROUP_MEMBER_URL = "https://console.tim.qq.com/v4/group_open_http_svc/add_group_member?";

    // 解散群组URL
    const DESTROY_GROUP_URL = "https://console.tim.qq.com/v4/group_open_http_svc/destroy_group?";

    // 删除群成员URL
    const DELETE_GROUP_MEMBER_URL = "https://console.tim.qq.com/v4/group_open_http_svc/delete_group_member?";

}