<?php


namespace CymTools\Im\Account;


use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $app)
    {
        $app['account'] = function ($app){
            return new Client($app);
        };
    }
}