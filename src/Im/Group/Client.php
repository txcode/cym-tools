<?php

namespace CymTools\Im\Group;

use CymTools\Im\Group\Bean\CreateGroupBean;
use CymTools\Im\Group\Bean\ModifyGroupBaseInfoBean;
use CymTools\Im\Support\Host;
use CymTools\Im\Support\TLSSigAPIv2;
use CymTools\Kernel\ServiceClient;
use CymTools\Kernel\Support\Str;
use GuzzleHttp\Client as Http;
use Psr\Http\Message\ResponseInterface;

class Client extends ServiceClient
{
    // 默认管理员账号ID
    private $identifier = 'administrator';

    // HTTP请求
    private $http;

    /**
     * @var TLSSigAPIv2
     */
    private $TLSSignAPI;


    public function __construct($app)
    {
        parent::__construct($app);
        $this->http = new Http(['verify' => false]);
        $this->TLSSignAPI = new TLSSigAPIv2($this->config->accessKeyId, $this->config->accessKeySecret);
    }


    /**
     * 创建群聊
     * @param CreateGroupBean $groupBean
     * @return ResponseInterface
     * @throws \Exception
     */
    public function createGroup(CreateGroupBean $groupBean): ResponseInterface
    {
        $url = $this->getUrlParameter(Host::CREATE_GROUP_URL);
        $result = $this->getBeanClassProperties($groupBean);
        return $this->http->post($url,['json'=>$result]);
    }

    /**
     * 修改群信息
     * @param ModifyGroupBaseInfoBean $baseInfoBean
     * @return ResponseInterface
     * @throws \Exception
     */
    public function modifyGroupBaseInfo(ModifyGroupBaseInfoBean $baseInfoBean): ResponseInterface
    {
        $url = $this->getUrlParameter(Host::MODIFY_GROUP_BASE_INFO_URL);
        $result = $this->getBeanClassProperties($baseInfoBean);
        return $this->http->post($url,['json'=>$result]);
    }


    /**
     * 添加群成员
     * @param $groupId
     * @param $memberList
     * @param int $silence 是否静默加人。0：非静默加人；1：静默加人。不填该字段默认为0
     * @return ResponseInterface
     * @throws \Exception
     */
    public function addGroupMember($groupId, $memberList, int $silence = 0): ResponseInterface
    {
        $url = $this->getUrlParameter(Host::ADD_GROUP_MEMBER_URL);
        $accountList = [];
        foreach ($memberList as $member){
            $accountList[] = ['Member_Account'=>$member];
        }
        return $this->http->post($url, ['json'=>['GroupId'=>$groupId, 'Silence'=>$silence,  'MemberList'=>$accountList] ]);
    }

    /**
     * 删除群成员
     * @param string $groupId 群ID
     * @param array $memberList 待删除的群成员
     * @param null $reason
     * @param int $silence
     * @return ResponseInterface
     * @throws \Exception
     */
    public function deleteGroupMember($groupId, $memberList, $reason=null, $silence = 0): ResponseInterface
    {
        $url = $this->getUrlParameter(Host::DELETE_GROUP_MEMBER_URL);
        $data = ['GroupId'=>$groupId, 'Silence'=>$silence, 'MemberToDel_Account'=>$memberList];
        if (!empty($reason) ){
            $data['Reason'] = $reason;
        }
        return $this->http->post($url, ['json'=>$data]);

    }

    /**
     * 解散群组
     * @param $groupId
     * @return ResponseInterface
     * @throws \Exception
     */
    public function destroyGroup($groupId): ResponseInterface
    {
        $url = $this->getUrlParameter(Host::DESTROY_GROUP_URL);
        return $this->http->post($url, ['json'=>['GroupId'=>$groupId] ]);
    }


    /**
     * 生成URL链接
     * @param $url string 请求链接
     * @param $user_id mixed|string 用户ID
     * @return string
     * @throws \Exception
     */
    private function getUrlParameter(string $url, string $user_id = null): string
    {
        $identifier = empty($user_id) ? $this->identifier: $user_id;
        $param = http_build_query([
            'sdkappid' => $this->config->accessKeyId,
            'identifier' => $identifier,
            'usersig' => $this->genUserSign($identifier),
            'random' => Str::randomInt(32),
            'contenttype' => 'json'
        ]);
        return $url.$param;
    }

    /**
     * 生成用户签名
     * @param $user_id string 用户ID
     * @return string
     * @throws \Exception
     */
    public function genUserSign(string $user_id)
    {
        return $this->TLSSignAPI->genUserSig($user_id);
    }
}