<?php

namespace CymTools\Im\Group\Bean;

/**
 * 群信息实体类
 * Class GroupBean
 * @package CymTools\Im\Support\bean
 */
class CreateGroupBean
{
    // 群主账号
    private $Owner_Account;

    // 群基础类型
    private $Type = 'Public';

    // 群组ID
    private $GroupId;

    // 群名称
    private $Name;

    // 群简介
    private $Introduction;

    // 群公告
    private $Notification;

    // 群头像
    private $FaceUrl;

    // 最大群成员数量
    private $MaxMemberCount = 50;

    // 入群方式
    private $ApplyJoinOption = 'NeedPermission';

    // 群组维度的自定义字段
    private $AppDefinedData = array();

    // 初始化群成员列表
    private $MemberList = array();

    // 群成员维度的自定义字段
    private $AppMemberDefinedData = array();

    /**
     * @return mixed
     */
    public function getOwnerAccount()
    {
        return $this->Owner_Account;
    }

    /**
     * @param mixed $Owner_Account
     */
    public function setOwnerAccount($Owner_Account): void
    {
        $this->Owner_Account = $Owner_Account;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->Type;
    }

    /**
     * @param string $Type
     */
    public function setType(string $Type): void
    {
        $this->Type = $Type;
    }

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->GroupId;
    }

    /**
     * @param mixed $GroupId
     */
    public function setGroupId($GroupId): void
    {
        $this->GroupId = $GroupId;
    }



    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name): void
    {
        $this->Name = $Name;
    }

    /**
     * @return mixed
     */
    public function getIntroduction()
    {
        return $this->Introduction;
    }

    /**
     * @param mixed $Introduction
     */
    public function setIntroduction($Introduction): void
    {
        $this->Introduction = $Introduction;
    }

    /**
     * @return mixed
     */
    public function getNotification()
    {
        return $this->Notification;
    }

    /**
     * @param mixed $Notification
     */
    public function setNotification($Notification): void
    {
        $this->Notification = $Notification;
    }

    /**
     * @return mixed
     */
    public function getFaceUrl()
    {
        return $this->FaceUrl;
    }

    /**
     * @param mixed $FaceUrl
     */
    public function setFaceUrl($FaceUrl): void
    {
        $this->FaceUrl = $FaceUrl;
    }

    /**
     * @return int
     */
    public function getMaxMemberCount(): int
    {
        return $this->MaxMemberCount;
    }

    /**
     * @param int $MaxMemberCount
     */
    public function setMaxMemberCount(int $MaxMemberCount): void
    {
        $this->MaxMemberCount = $MaxMemberCount;
    }

    /**
     * @return string
     */
    public function getApplyJoinOption(): string
    {
        return $this->ApplyJoinOption;
    }

    /**
     * @param string $ApplyJoinOption
     */
    public function setApplyJoinOption(string $ApplyJoinOption): void
    {
        $this->ApplyJoinOption = $ApplyJoinOption;
    }

    /**
     * @return array
     */
    public function getAppDefinedData(): array
    {
        return $this->AppDefinedData;
    }

    /**
     * @param array $AppDefinedData
     */
    public function setAppDefinedData(array $AppDefinedData): void
    {
        $this->AppDefinedData = $AppDefinedData;
    }

    /**
     * @return array
     */
    public function getMemberList(): array
    {
        return $this->MemberList;
    }

    /**
     * @param array $MemberList
     */
    public function setMemberList(array $MemberList): void
    {
        $this->MemberList = $MemberList;
    }

    /**
     * @return array
     */
    public function getAppMemberDefinedData(): array
    {
        return $this->AppMemberDefinedData;
    }

    /**
     * @param array $AppMemberDefinedData
     */
    public function setAppMemberDefinedData(array $AppMemberDefinedData): void
    {
        $this->AppMemberDefinedData = $AppMemberDefinedData;
    }




}