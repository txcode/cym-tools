<?php

namespace CymTools\Im\Group\Bean;

class ModifyGroupBaseInfoBean
{
    // 需要修改的群组ID
    private $GroupId;

    // 群名称
    private $Name;

    // 群简介
    private $Introduction;

    // 群公告
    private $Notification;

    // 群头像
    private $FaceUrl;

    // 最大群成员数量
    private $MaxMemberNum;

    // 申请加群处理方式
    private $ApplyJoinOption;

    // 开通群组维度的自定义字段
    private $AppDefinedData = array();

    // 群内群成员禁言，只有群管理员和群主以及系统管理员可以发言
    private $ShutUpAllMember = array();

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->GroupId;
    }

    /**
     * @param mixed $GroupId
     */
    public function setGroupId($GroupId): void
    {
        $this->GroupId = $GroupId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name): void
    {
        $this->Name = $Name;
    }

    /**
     * @return mixed
     */
    public function getIntroduction()
    {
        return $this->Introduction;
    }

    /**
     * @param mixed $Introduction
     */
    public function setIntroduction($Introduction): void
    {
        $this->Introduction = $Introduction;
    }

    /**
     * @return mixed
     */
    public function getNotification()
    {
        return $this->Notification;
    }

    /**
     * @param mixed $Notification
     */
    public function setNotification($Notification): void
    {
        $this->Notification = $Notification;
    }

    /**
     * @return mixed
     */
    public function getFaceUrl()
    {
        return $this->FaceUrl;
    }

    /**
     * @param mixed $FaceUrl
     */
    public function setFaceUrl($FaceUrl): void
    {
        $this->FaceUrl = $FaceUrl;
    }

    /**
     * @return mixed
     */
    public function getMaxMemberNum()
    {
        return $this->MaxMemberNum;
    }

    /**
     * @param mixed $MaxMemberNum
     */
    public function setMaxMemberNum($MaxMemberNum): void
    {
        $this->MaxMemberNum = $MaxMemberNum;
    }

    /**
     * @return mixed
     */
    public function getApplyJoinOption()
    {
        return $this->ApplyJoinOption;
    }

    /**
     * @param mixed $ApplyJoinOption
     */
    public function setApplyJoinOption($ApplyJoinOption): void
    {
        $this->ApplyJoinOption = $ApplyJoinOption;
    }

    /**
     * @return array
     */
    public function getAppDefinedData(): array
    {
        return $this->AppDefinedData;
    }

    /**
     * @param array $AppDefinedData
     */
    public function setAppDefinedData(array $AppDefinedData): void
    {
        $this->AppDefinedData = $AppDefinedData;
    }

    /**
     * @return array
     */
    public function getShutUpAllMember(): array
    {
        return $this->ShutUpAllMember;
    }

    /**
     * @param array $ShutUpAllMember
     */
    public function setShutUpAllMember(array $ShutUpAllMember): void
    {
        $this->ShutUpAllMember = $ShutUpAllMember;
    }



}