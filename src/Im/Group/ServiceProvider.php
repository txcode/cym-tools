<?php

namespace CymTools\Im\Group;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {
        $app['group'] = function ($app){
            return new Client($app);
        };
    }
}