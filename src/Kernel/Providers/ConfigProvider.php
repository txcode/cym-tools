<?php


namespace CymTools\Kernel\Providers;


use CymTools\Kernel\Config;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ConfigProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $app)
    {
        $app['config'] = function ($app){
            return new Config($app->getConfig());
        };
    }
}