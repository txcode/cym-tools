<?php


namespace CymTools\Kernel;


use CymTools\Kernel\Providers\ConfigProvider;
use Pimple\Container;

class ServiceContainer extends Container
{

    protected $id;

    protected $providers;

    protected $userConfig;

    public function __construct(array $config = [], array $prepends = [], string $id = null)
    {

        $this->registerProviders($this->getProviders() );

        $this->id = $id;

        $this->userConfig = $config;

        parent::__construct($prepends);
    }


    public function getConfig()
    {
        return $this->userConfig;
    }

    public function getProviders()
    {
        return array_merge([
            ConfigProvider::class
        ], $this->providers);
    }

    public function registerProviders(array $providers)
    {
        foreach ($providers as $provider){
            parent::register(new $provider());
        }
    }

    public function __get($name)
    {
        return $this->offsetGet($name);
    }
}