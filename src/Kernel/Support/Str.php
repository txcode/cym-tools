<?php


namespace CymTools\Kernel\Support;


class Str
{
    public static function studly($value)
    {
        $value = ucwords(str_replace(['-', '_'], ' ', $value));

        return str_replace(' ', '', $value);
    }

    /**
     * 生成随机整数
     * @param $length
     * @return string
     */
    public static function randomInt($length)
    {
        $z="";
        for($i=0;$i<$length/4;$i++){
            $z .=rand(1000,9999);
        }
        return $z;
    }

    /**
     * 生成随机数
     * @param string $type
     * @param int $len
     * @return false|string
     */
    public static function random($type = 'alnum', $len = 8)
    {
        switch ($type) {
            case 'alpha':
                $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'alnum':
                $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'numeric':
                $pool = '0123456789';
                break;
            case 'nozero':
                $pool = '123456789';
                break;
        }
        return substr(str_shuffle(str_repeat($pool, ceil($len / strlen($pool)))), 0, $len);
    }
}