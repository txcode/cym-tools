<?php


namespace CymTools\Kernel;


class Config
{
    public function __construct(array $items)
    {
        foreach ($items as $ik => $item) {
            if (is_array($item) ){
                foreach ($item as $key => $value) {
                    $this->set($key, $value);
                }
                unset($item);
            }else{
                $this->set($ik, $item);
            }
        }
    }

    public function set($key, $value)
    {
        $this->$key = $value;
    }

    public function get($key)
    {
        return $this->$key;
    }
}