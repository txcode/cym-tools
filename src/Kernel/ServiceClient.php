<?php

namespace CymTools\Kernel;

class ServiceClient
{

    protected $app;

    /**
     * @var Config
     */
    protected $config;

    public function __construct($app)
    {
        $this->app = $app;
        $this->config = $this->app['config'];
    }


    public function getBeanClassProperties($instance): array
    {
        $classData = [];
        $reflection = new \ReflectionClass($instance);
        $properties = $reflection->getProperties();
        foreach ($properties as $property){
            $property->setAccessible(true);
            if ($property->getValue($instance)){
                $classData[$property->getName()] = $property->getValue($instance);
            }
        }
        return $classData;
    }
}