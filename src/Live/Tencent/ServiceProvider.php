<?php

namespace CymTools\Live\Tencent;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['tencent'] = function ($app){
            return new Client($app);
        };
    }
}