<?php

namespace CymTools\Live\Tencent;

use CymTools\Kernel\ServiceClient;
use GuzzleHttp\Client as Http;

class Client extends ServiceClient
{

    /**
     * @var Http
     */
    private $http;

    public function __construct($app)
    {
        $this->http = new Http(['verify' => false]);

        parent::__construct($app);
    }


    /**
     * 生成流地址
     * @param $domain
     * @param $streamName
     * @param null $key
     * @param null $time
     * @return string
     */
    public function getPushUrl($domain, $streamName, $key = null, $time = null)
    {
        if($key && $time){
            $txTime = strtoupper(base_convert(strtotime($time),10,16));
            //txSecret = MD5( KEY + streamName + txTime )
            $txSecret = md5($key.$streamName.$txTime);
            $ext_str = "?".http_build_query(array(
                    "txSecret"=> $txSecret,
                    "txTime"=> $txTime
                ));
        }
        return "rtmp://".$domain."/live/".$streamName . (isset($ext_str) ? $ext_str : "");
    }
}