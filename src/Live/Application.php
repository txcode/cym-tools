<?php

namespace CymTools\Live;

use CymTools\Kernel\ServiceContainer;

/**
 * 腾讯云直播
 * @property Tencent\Client $tencent
 * @property MLVBLiveRoom\Client $liveRoom
 */
class Application extends ServiceContainer
{
    protected $providers = [
        Tencent\ServiceProvider::class,
        MLVBLiveRoom\ServiceProvider::class
    ];
}