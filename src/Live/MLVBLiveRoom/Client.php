<?php

namespace CymTools\Live\MLVBLiveRoom;
use CymTools\Kernel\ServiceClient;
use CymTools\Kernel\Support\Str;
use GuzzleHttp\Client as Http;
use CymTools\Im\Support\TLSSigAPIv2;
use Psr\Http\Message\ResponseInterface;

class Client extends ServiceClient
{
    // 默认liveRoom请求地址
    const HOST = "https://liveroom.qcloud.com/weapp/live_room/";

    private $TLSSignAPI;

    private $http;

    /**
     * 用户授权Token
     * @var string
     */
    private $token;

    public function __construct($app)
    {
        parent::__construct($app);
        $this->http = new Http(['verify'=>false]);
        $this->TLSSignAPI = new TLSSigAPIv2($this->config->sdkAppId, $this->config->secretKey);
    }

    /**
     * 登陆账号
     * @param string $userID
     * @return ResponseInterface
     */
    public function login(string $userID): ResponseInterface
    {
        $param = http_build_query([
            'sdkAppID' => $this->config->sdkAppId,
            'userID' => $userID,
            'userSig' => $this->TLSSignAPI->genUserSig($userID),
        ]);
        $url = self::HOST."login?".$param;
        return $this->http->post($url);
    }

    /**
     * 登出账号
     * @param string $roomID 房间ID
     * @param string $userID 用户ID
     * @return ResponseInterface
     */
    public function logout(string $roomID, string $userID): ResponseInterface
    {
        $url = $this->getUrlParameter("logout", $userID);
        return $this->http->post($url,['json'=>['roomID'=>$roomID, 'userID'=>$userID] ]);
    }
    
    

    /**
     * 创建直播房间
     * @param string $userID 用户ID
     * @param string $roomInfo 房间信息
     * @param string|null $roomId 房间号 可为空 如果填写则表示由您来决定房间ID
     * @return ResponseInterface
     */
    public function createRoom(string $userID, string $roomInfo, string $roomId = null): ResponseInterface
    {
        $requestData = ['userID' => $userID, 'roomInfo' => $roomInfo];
        if (!empty($roomId)){
            $requestData['roomID'] = $roomId;
        }

        $url = self::HOST."create_room?userID={$userID}&token={$this->token}";

        return $this->http->post($url, ['json'=>$requestData]);
    }

    /**
     * 销毁直播房间
     * @param string $roomID 房间ID
     * @param string $userID 用户ID
     * @return ResponseInterface
     */
    public function destroyRoom(string $roomID, string $userID): ResponseInterface
    {
        $url = $this->getUrlParameter("destroy_room",$userID);
        return $this->http->post($url, ['json'=>['roomID'=>$roomID, 'userID'=>$userID] ]);
    }


    /**
     * 获取直播房间列表
     * @param string $userID 用户ID
     * @param int $cnt 期待的房间个数
     * @param int $index 期待的房间索引的开始位置
     * @return ResponseInterface
     */
    public function getRoomList(string $userID, int $cnt, int $index): ResponseInterface
    {
        $url = $this->getUrlParameter("get_room_list", $userID);
        return $this->http->post($url, ['json'=>['cnt'=>$cnt, 'index'=>$index] ]);
    }

    /**
     * 获取直播推流地址
     * @param string $userID 用户ID
     * @return ResponseInterface
     */
    public function getAnchorUrl(string $userID): ResponseInterface
    {
        $url = $this->getUrlParameter("get_anchor_url", $userID);
        return $this->http->post($url, ['json'=>['userID'=>$userID] ]);
    }

    /**
     * 加入直播间成为主播/小主播
     * @param string $roomID 房间ID
     * @param string $userID 用户ID
     * @param string $userName 用户昵称
     * @param string $pushURL 推流URL
     * @param string $userAvatar 用户头像
     * @return ResponseInterface
     */
    public function addAnchor(string $roomID, string $userID, string $userName, string $pushURL, string $userAvatar): ResponseInterface
    {
        $url = $this->getUrlParameter("add_anchor", $userID);
        return $this->http->post($url, ['json'=>[
            'roomID' => $roomID,
            'userID' => $userID,
            'userName' => $userName,
            'pushURL' => $pushURL,
            'userAvatar' => $userAvatar
        ]]);
    }

    /**
     * 进入直播间成为观众
     * @param string $roomID 房间ID
     * @param string $userID 用户ID
     * @param string $userInfo 用户信息 可自定义为JSON
     * @return ResponseInterface
     */
    public function addAudience(string $roomID, string $userID, string $userInfo): ResponseInterface
    {
        $url = $this->getUrlParameter("add_audience ", $userID);
        return $this->http->post($url, ['json'=>[
            'roomID' => $roomID,
            'userID' => $userID,
            'userInfo' => $userInfo
        ]]);
    }

    /**
     * 观众退出直播间
     * @param string $roomID 房间ID
     * @param string $userID 用户ID
     * @return ResponseInterface
     */
    public function deleteAudience(string $roomID, string $userID): ResponseInterface
    {
        $url = $this->getUrlParameter("delete_audience", $userID);
        return $this->http->post($url, ['json'=>[
            'roomID' => $roomID,
            'userID' => $userID,
        ]]);
    }
    
    /**
     * 获取房间信息
     * @param string $roomId 房间ID
     * @param string $userID 用户ID
     * @return ResponseInterface
     */
    public function getAnchors(string $roomId, string $userID): ResponseInterface
    {
        $url = $this->getUrlParameter("get_anchors", $userID);
        return $this->http->post($url, ['json'=>['roomID'=>$roomId] ]);
    }

    /**
     * 获取观众列表和观众人数
     * @param string $roomId
     * @param string $userID
     * @return ResponseInterface
     */
    public function getAudiences(string $roomId, string $userID): ResponseInterface
    {
        $url = $this->getUrlParameter("get_audiences", $userID);
        return $this->http->post($url, ['json'=>['roomID'=>$roomId] ]);
    }

    /**
     * 生成URL路径
     * @param string $action
     * @param string $userID
     * @return string
     */
    private function getUrlParameter(string $action, string $userID): string
    {
        return self::HOST."{$action}?userID={$userID}&token={$this->token}";
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): Client
    {
        $this->token = $token;
        return $this;
    }

}