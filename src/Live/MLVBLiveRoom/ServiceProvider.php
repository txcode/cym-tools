<?php

namespace CymTools\Live\MLVBLiveRoom;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['liveRoom'] = function ($app){
            return new Client($app);
        };
    }
}