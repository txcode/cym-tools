<?php

namespace CymTools;


use CymTools\Kernel\Support\Str;

/**
 * Class Factory
 * @method static \CymTools\Sms\Application sms(array $config = [])
 * @method static \CymTools\Im\Application im(array $config = [])
 * @method static \CymTools\Live\Application live(array $config = [])
 */
class Factory
{

    public static function __callStatic($name, $arguments)
    {
        return self::make($name, $arguments);
    }

    public static function make($name, array $config)
    {
        $namespace = Str::studly($name);
        $application =  "\\CymTools\\{$namespace}\Application";
        return new $application($config);
    }
}